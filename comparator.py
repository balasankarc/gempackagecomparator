import urllib
import json
import re
import sys
from jinja2 import Environment, FileSystemLoader


def compare(version1, version2):
    '''Comparing two version strings after normalization'''
    def normalize(v):
        return [int(x) for x in re.sub(r'(\.0+)*$', '', v).split(".")]
    return cmp(normalize(version1), normalize(version2))

if len(sys.argv) == 1:
    print "Usage : python comparator.py <dot file>"
    sys.exit()
dotfile = open(sys.argv[1])
gemlist = []
debian = {}
ruby = {}
status = {}
nonlist = ["rails"]                                                                 #Packages without ruby- suffix
assetlist = ["rails-assets-perfect-scrollbar"]                                      #Packages not from RubyGems
print "======================================================================="
for line in dotfile.readlines():
    if not "->" in line and not "{" in line and not "}" in line:
        gemlist.append(line[1:line.find('"', 1)])
for gem in gemlist:
    print gem
    gemdebian = gem
    if gem not in nonlist:
        gemdebian = "ruby-"+gemdebian
    if gem not in assetlist:
        deburl = "http://sources.debian.net/api/src/"+gemdebian.replace("_", "-")   #Fetching JSON using Debian API
    else:
        continue
    print deburl
    debresponse = urllib.urlopen(deburl)
    debdata = json.loads(debresponse.read())
    try:
        debversionorig = debdata[u'versions'][-1][u'version']
        debversion = debversionorig[0:debversionorig.index('-')]                    #Strip Debian version suffix
        if ":" in str(debversion):
            debversion = debversion[debversion.index(":")+1:]                       #Strip ":" at the beginning - rails
        position = re.search("[~+]", debversion)                                    #~dfsg and +gh versions
        if position:
            debversion = debversion[:position.start()]
    except:
        debversion = "NIL"
    debian[gem] = debversion
    rubyurl = "https://rubygems.org/api/v1/versions/"+gem+".json"                   #Fetching json using RubyGems API
    print rubyurl
    rubyresponse = urllib.urlopen(rubyurl)
    rubydata = json.loads(rubyresponse.read())
    rubyversion = rubydata[0][u'number']
    ruby[gem] = rubyversion
    print "Debian Version : " + str(debversion)
    print "Ruby Version : " + str(rubyversion)
    print "======================================================================="
    if debversion == "NIL" or rubyversion == "NIL":
        status[gem] = "grey"
        continue
    else:
        try:
            result = compare(debversion, rubyversion)
        except:
            result = -1
        if result == 0:
            status[gem] = "success"
        elif result > 0:
            status[gem] = "warning"
        else:
            status[gem] = "error"

env = Environment(loader=FileSystemLoader('templates'))
template = env.get_template('main.html')                                            # Load HTML page template
render = template.render(locals())
with open("index.html", "w") as file:
    file.write(render)
